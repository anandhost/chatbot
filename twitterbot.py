from chatterbot import ChatBot
from setting import TWITTER
import logging

logging.basicConfig(level=logging.INFO)

chatbot = ChatBot("TwitterBot",
    storage_adapter="chatterbot.storage.JsonFileStorageAdapter",
    logic_adapters=[
        "chatterbot.logic.BestMatch"
    ],
    input_adapter="chatterbot.input.TerminalAdapter",
    output_adapter="chatterbot.output.TerminalAdapter",
    database="./twitter-database.db",

    twitter_consumer_key=TWITTER["CONSUMER_KEY"],
    twitter_consumer_secret=TWITTER["CONSUMER_SECRET"],
    twitter_access_token_key=TWITTER["ACCESS_TOKEN"],
    twitter_access_token_secret=TWITTER["ACCESS_TOKEN_SECRET"],
    trainer="chatterbot.trainers.TwitterTrainer"

)

chatbot.train()

chatbot.logger.info('Trained database generated successfully!')
print("Bot : Let's Talk Now")


while True:
    try:
        print("bot:")
        bot_input = chatbot.get_response(None)
    except (KeyboardInterrupt, EOFError, SystemExit):
        break
